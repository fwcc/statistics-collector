statistics\_collector.metrics package
=====================================

Submodules
----------

statistics\_collector.metrics.gitlab\_active\_users module
----------------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_active_users
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.gitlab\_api module
------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_api
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.gitlab\_ci\_jobs module
-----------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_ci_jobs
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.gitlab\_mail\_domains module
----------------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_mail_domains
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.gitlab\_merge\_requests module
------------------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_merge_requests
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.gitlab\_project\_stats module
-----------------------------------------------------------

.. automodule:: statistics_collector.metrics.gitlab_project_stats
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.hifis\_gpu\_stats module
------------------------------------------------------

.. automodule:: statistics_collector.metrics.hifis_gpu_stats
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.mattermost\_stats module
------------------------------------------------------

.. automodule:: statistics_collector.metrics.mattermost_stats
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.metric module
-------------------------------------------

.. automodule:: statistics_collector.metrics.metric
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.overleaf\_active\_projects module
---------------------------------------------------------------

.. automodule:: statistics_collector.metrics.overleaf_active_projects
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.overleaf\_active\_users module
------------------------------------------------------------

.. automodule:: statistics_collector.metrics.overleaf_active_users
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.overleaf\_api module
--------------------------------------------------

.. automodule:: statistics_collector.metrics.overleaf_api
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.overleaf\_mail\_domains module
------------------------------------------------------------

.. automodule:: statistics_collector.metrics.overleaf_mail_domains
   :members:
   :undoc-members:
   :show-inheritance:

statistics\_collector.metrics.overleaf\_user\_origins module
------------------------------------------------------------

.. automodule:: statistics_collector.metrics.overleaf_user_origins
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: statistics_collector.metrics
   :members:
   :undoc-members:
   :show-inheritance:

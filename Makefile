.DEFAULT_GOAL := build
.PHONY: build lint test docs package deploy
PROJ_SLUG = statistics_collector
CLI_NAME = statistics-collector
SHELL = bash

build:
	poetry build

lint:
	poetry run black --check .
	poetry run isort --check .

test:
	poetry run pytest tests/

docs:
	poetry run sphinx-apidoc -f -o ./docs/source/ ./statistics_collector
	cd docs && poetry run make html

package: clean docs
	poetry build

deploy:
	poetry run statistics-collector

clean :
	rm -rf .pytest_cache \
	rm -rf dist \
	rm -rf docs/build \
	rm -rf *.egg-info \
	rm -rf docs/source/modules \
	rm -rf htmlcov \
	rm -rf meta\
	rm -rf output

reformat:
	poetry run isort .
	poetry run black .

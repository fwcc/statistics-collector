import datetime
import json
import sys
from typing import Optional

import pandas as pd
import requests

from .metric import Metric


class GitlabMergeRequests(Metric):
    """Get amount of merge requests

    Query GitLab via its GraphQL API and convert the results.

    Examples
    --------
    Example content of pd.DataFrame returned:
        count,identifier,recordedAt
        17183,MERGE_REQUESTS,2022-08-01T23:50:05+02:00
    """

    def init(self):
        self.uri = f"https://{self.settings['HOST_GITLAB']}/api/graphql"
        self.headers = {"Authorization": "Bearer " + self.settings["TOKEN_GITLAB"]}
        # TODO ! Magic strings -> Token !

    def _build_query(
        self,
        recordedAfter: str,
        recordedBefore: str,
        identifier="MERGE_REQUESTS",
        after: Optional[str] = None,
    ) -> str:
        """Builds custom graphql query

        Parameters
        ----------
        recordedAfter : Start of the day "date"
        recordedBefore : End of the day "date"
        identifier : object identifier for GraphQL API
        after : denotes endCursor, needed for navigating through multiple pages

        Returns
        -------
        Valid GraphQL query as String
        """

        projects_argument = f'(,after: "{after}")' if after is not None else ""

        # TODO Find a way to break long string in code but not in actual output
        return f"""
        {{
            usageTrendsMeasurements(identifier: {identifier},recordedAfter: \"{recordedAfter}\",recordedBefore: \"{recordedBefore}\"{projects_argument}) {{
                pageInfo {{
                    endCursor
                    startCursor
                    hasNextPage
                }}
                nodes {{
                    count
                    identifier
                    recordedAt
                }}
            }}
        }}"""

    def _run_query(self, query) -> json:
        requested_data = requests.post(
            self.uri, json={"query": query}, headers=self.headers
        )
        requested_data.raise_for_status()
        return requested_data.json()

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        """Get data on merge requests

        Parameters
        ----------
        date : None as default --> getting data for yesterday

        Returns
        -------
        pd.DataFrame
            count,identifier,recordedAt

        Notes
        -----
        d = datetime.date.fromisoformat('2021-12-04')
        # --> d is now datetime.date(2021, 12, 4)
        """

        # TODO option for conversion of user input to datetime.datetime.date needs to be
        # added when either CLI or config is implemented

        # Start of the day "date" - format in order to fit query requirements
        # e.g: "2021-01-01T00:00:00+00:00"
        recorded_after = datetime.datetime.combine(date, datetime.datetime.min.time())
        recorded_after_str = recorded_after.strftime("%Y-%m-%dT%H:%M:%S")
        # End of the day "date"
        recorded_before = recorded_after + datetime.timedelta(days=1)
        recorded_before_str = recorded_before.strftime("%Y-%m-%dT%H:%M:%S")

        # Sanity checks for formatting
        # Print(f"recorded_after {recorded_after_str}")
        # Print(f"recorded_before {recorded_before_str}")

        init_query = self._build_query(
            recordedAfter=recorded_after_str, recordedBefore=recorded_before_str
        )
        q_response = self._run_query(init_query)

        # Pretty output for testing
        # print(json.dumps(q_response, indent=2))
        hasNextPage = q_response["data"]["usageTrendsMeasurements"]["pageInfo"][
            "hasNextPage"
        ]
        endCursor = q_response["data"]["usageTrendsMeasurements"]["pageInfo"][
            "endCursor"
        ]

        content = pd.DataFrame()
        # Pick relevant fields from response and remove pandas index
        filtered_response = pd.DataFrame(
            q_response["data"]["usageTrendsMeasurements"]["nodes"]
        )
        result = pd.concat([content, filtered_response], ignore_index=True)

        while hasNextPage:
            current_query = self._build_query(
                recordedAfter=recorded_after_str,
                recordedBefore=recorded_before_str,
                after=endCursor,
            )
            q_response = self._run_query(current_query)
            hasNextPage = q_response["data"]["usageTrendsMeasurements"]["pageInfo"][
                "hasNextPage"
            ]
            endCursor = q_response["data"]["usageTrendsMeasurements"]["pageInfo"][
                "endCursor"
            ]
            filtered_response = pd.DataFrame(
                q_response["data"]["usageTrendsMeasurements"]["nodes"]
            )
            result = pd.concat([content, filtered_response], ignore_index=True)

        return result

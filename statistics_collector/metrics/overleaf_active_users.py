import datetime
import sys
from typing import Optional

import pandas as pd
import requests

from .overleaf_api import OverleafApiMetric


class OverleafActiveUsers(OverleafApiMetric):
    """Gets amount of active users

    The API of overleaf-statistics is called, which returns the statistics in the needed way.

    Examples
    --------
    Example content of pandas DataFrame returned (splitted for length):
        datetime,number_of_users,last_active_1_days,last_active_7_days,
        last_active_14_days,last_active_30_days,last_active_60_days
        2022-05-11T10:03:40.432770+00:00,7447,416,1431,2093,2909,3619
    """

    TOKEN_LAST_ACTIVITY = "last_activity_on"

    def init(self):
        # Currently set static as in original codebase, will be refactored
        self.activity_period = [1, 7, 14, 30, 60]

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        """Determines active users for different timeframes

        Parameters
        ----------
        date : None as default --> getting data for yesterday

        Returns
        -------
        pd.DataFrame : temporal distribution of active users
            datetime,number_of_users,last_active_1_days,last_active_7_days,
            last_active_14_days,last_active_30_days,last_active_60_days
        """

        if date is None:
            print("[INFO] No date specified. Getting data for yesterday")
            date = datetime.date.today() - datetime.timedelta(days=1)

        end_time = int(
            datetime.datetime.combine(date, datetime.datetime.min.time()).timestamp()
        )
        current_datetime = datetime.datetime.now(datetime.timezone.utc)

        # Get the number of all users from the overleaf-statistics API.
        total_users_int = self.call_api("count_active_users")["number_of_users"]

        total_users = pd.Series(
            [current_datetime.isoformat(), total_users_int],
            index=["datetime", "number_of_users"],
        )

        # Get the number of users that have been logged in in a given period of time from the overleaf-statistics API.
        last_active_dict = {
            f"last_active_{e}_days": self.call_api(
                "count_active_users",
                params={"duration": e * 60 * 60 * 24, "end_time": end_time},
            )["number_of_users"]
            for e in self.activity_period
        }
        last_active = pd.Series(
            last_active_dict.values(), index=last_active_dict.keys()
        )

        return pd.concat([total_users, last_active]).to_frame().T

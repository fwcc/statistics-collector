import gitlab

from .metric import Metric


class GitlabApiMetric(Metric):
    """Abstract class providing GitLab session.

    Attributes
    ----------
    settings : a dictionary that must contain GitLab API access credentials - {'HOST_GITLAB': <hostname>, 'TOKEN_GITLAB': <access token>}
    """

    def gl_api(self) -> gitlab.Gitlab:
        """
        Authenticates to the GitLab API and returns a GitLab API session object.
        """
        server_url = f"https://{self.settings['HOST_GITLAB']}"
        gl = gitlab.Gitlab(
            server_url,
            pagination="keyset",
            order_by="id",
            private_token=self.settings["TOKEN_GITLAB"],
        )
        gl.auth()
        return gl

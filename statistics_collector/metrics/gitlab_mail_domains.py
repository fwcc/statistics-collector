import datetime
from typing import Optional

import pandas as pd

from .gitlab_api import GitlabApiMetric


class GitlabMailDomains(GitlabApiMetric):
    """Retrieve the count of users per email domain."""

    def get_data(self) -> pd.DataFrame:
        """Return the count of users per email domain as pandas dataframe."""
        gl = self.gl_api()

        # list(iterator=True) returns a generator object.
        # This is Gitlabs API recommended way to iterate through a large
        # number of items.
        users = gl.users.list(iterator=True, per_page=250)

        # Get most recent date of activity from each user
        mail_domain_list = [
            user.attributes["email"].split("@")[1] for user in list(users)
        ]

        mail_domain_df = pd.DataFrame(mail_domain_list, columns=["domains"])

        mail_domain_pivot_df = mail_domain_df.pivot_table(
            index="domains", aggfunc="size"
        ).sort_values(ascending=False)

        prepend = pd.Series(
            [datetime.datetime.now(datetime.timezone.utc).isoformat()],
            index=["datetime"],
        )

        result = pd.concat([prepend, mail_domain_pivot_df]).to_frame().T

        return result

"""This module handles cli interface, execution and saving the results"""

import os
import re
from pathlib import Path

import click
import pandas as pd
from dotenv import load_dotenv

from .metrics.gitlab_active_users import GitlabActiveUsers
from .metrics.gitlab_ci_jobs import GitlabCIJobs
from .metrics.gitlab_mail_domains import GitlabMailDomains
from .metrics.gitlab_merge_requests import GitlabMergeRequests
from .metrics.gitlab_project_stats import GitlabProjectStats
from .metrics.hifis_gpu_stats import HifisGpuStats
from .metrics.mattermost_stats import MattermostStats
from .metrics.overleaf_active_projects import OverleafActiveProjects
from .metrics.overleaf_active_users import OverleafActiveUsers
from .metrics.overleaf_mail_domains import OverleafEmailDomains
from .metrics.overleaf_user_origins import OverleafUserOrigins

# TODO rename runner.py
# Manage sensitive credentials through env variables
load_dotenv()

# Load credentials from local .env file
settings = os.environ


def read_csv(file_location):
    if not file_location.is_file():
        return pd.DataFrame()
    try:
        return pd.read_csv(file_location, sep=";", index_col=False)
    except pd.errors.EmptyDataError:
        pass
    return pd.DataFrame()


def write_csv(filename, df_data):
    current_path = Path.cwd()
    output_path = Path(current_path, "output")
    Path(output_path).mkdir(parents=True, exist_ok=True)
    file_location = output_path / filename

    # Concatenate dataframes if file exists to allow handling of new columns
    data_present = read_csv(file_location)
    df_concat = pd.concat([data_present, df_data], ignore_index=True, sort=False)
    # Ensure columns are ordered according to most recent data
    df_concat = df_concat[df_data.columns]

    df_concat.to_csv(
        file_location,
        mode="w",
        index=False,
        sep=";",
        na_rep="NaN",
    )


def exec_stats(metric_class, settings):
    metric = metric_class(settings)
    # Remodelling the classname into a fitting filename
    file_name = metric.__class__.__name__
    # Finding and splitting at uppercase letters, add _after each
    file_name = re.sub(r"([A-Z][^A-Z]*)", r"\1_", file_name).split()
    # Removing last underscore and add .csv ending
    file_name = f"{file_name[0][:-1]}.csv"
    file_name = file_name.lower()
    df_data = metric.get_data_wrapper()
    write_csv(file_name, df_data)


dict_metrics = {
    "mattermost_stats": MattermostStats,
    "gitlab_active_users": GitlabActiveUsers,
    "gitlab_ci_jobs": GitlabCIJobs,
    "gitlab_merge_requests": GitlabMergeRequests,
    "gitlab_project_stats": GitlabProjectStats,
    "gitlab_mail_domains": GitlabMailDomains,
    "overleaf_active_users": OverleafActiveUsers,
    "overleaf_active_projects": OverleafActiveProjects,
    "overleaf_user_origins": OverleafUserOrigins,
    "overleaf_mail_domains": OverleafEmailDomains,
    "hifis_gpu_stats": HifisGpuStats,
}


# Multiple=True offers chaining, so granular choices are possible.
# If no metric is specified, all statistics are retreived.
# TODO format help
@click.command()
@click.option(
    "--metric",
    "-m",
    default=dict_metrics.keys(),
    help=f"""Choose one or more metrics.
Can be any of: {", ".join(dict_metrics.keys())}.
Default: ALL.
Example: --> choose two metrics: -m mattermost_stats -m gitlab_active_users""",
    multiple=True,
)
def menu(metric):
    print("Start retrieving statistics...")
    for m in metric:
        if m not in dict_metrics:
            print(f"ERROR: Metric {m} is not known.")
            continue
        exec_stats(dict_metrics[m], settings)
        print(f"Successfully retrieved {m}.")


if __name__ == "__main__":
    menu()

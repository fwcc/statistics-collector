# HIFIS statistics collector

## What is the software intended for?

Statistics collector is able to collect data from different sources and prepare it for further usage in statistical analysis.
This tool was originally created for helping with the yearly HIFIS report but has been re-written in a way that allows for covering a broader spectrum of use-cases.
As for now, statistical data is retrieved from HZDR's `GitLab`, `Mattermost` and `Overleaf` services.

Current status: Working state.

## Table of Content

- [HIFIS statistics collector](#hifis-statistics-collector)
  - [What is the software intended for?](#what-is-the-software-intended-for)
  - [Table of Content](#table-of-content)
    - [Installation](#installation)
    - [Usage](#usage)
  - [For developers](#for-developers)
  - [Examples](#examples)
  - [Debugging](#debugging)
    - [To-Dos](#to-dos)
  - [Resources](#resources)
  - [Author Information](#author-information)

### Installation

Clone the repository and initialize your working environment:

```shell
git clone https://gitlab.hzdr.de/fwcc/statistics-collector.git
cd statistics-collector/
poetry install
```

We made the choice to use [Poetry](https://python-poetry.org/) and will therefor reference it in the further documentation - in case you decide to use a different environment manager, you can find the dependencies in `pyproject.toml`.
Please refer to the [poetry documentation](https://python-poetry.org/docs/) for further information.

After poetry has finished, cd into `statistics_collector/` and execute `poetry run statistics-collector --help`

Poetry installs some packages that are required for performing quality checks.
These checks can be performed automatically via ci-pipelines, but can also be executed locally - and should, if no ci-pipeline is configured yet.

It's considered good practice to check your code before pushing it with following commands:

```shell
$ cd statistics_collector/
# Order your imports
$ isort -rc .
# Have a code-check
$ flake8 .
```

### Usage

#### Credentials and tokens

If you decide to work via command line interface, credentials have to be provided as environment variables e. g. within a local `.env` file on your machine. An example draft can be found in `templates/`. This ensures sensitive credentials stay on your machine. In case you decide to extend the code to fit your needs, please make sure `.env` is included within your `.gitignore`.

The tool comes with GitLab-CI integration, per default statistics are generated once per day and stored in `output/`


#### Start statistics-collector from Command-Line-Interface

Follow the steps mentioned in [Credentials and tokens](#credentials-and-tokens)

To run the program you can call one of these two alternatives:

1. Call the program as a Poetry script/executable:

```bash
poetry run statistics-collector
```

2. Call the program as a Python package:

```bash
poetry run python3 -m statistics_collector.generate_statistics
```

Executing `poetry run statistics-collector` will retrieve all existing metrics by default.
If you want to retrieve only a specific metric, first execute `poetry run statistics-collector --help`, which will list you
all currently available metrics under 'Options'. Example:

```
Options:
-m, --metric TEXT Choose one or more metrics. Can be any of:
mattermost_stats, gitlab_active_users, gitlab_ci_jobs,
gitlab_merge_requests, gitlab_project_stats.
```

You can now specify the metric(s) of your choice with `-m`.
Example: --> choose two metrics:

```
python generate_statistics.py -m mattermost_stats -m gitlab_active_users
```

Results are saved as '.csv' files at  '/output'.

## For developers

![Class diagram](docs/img/class_diagram.svg)

The package is designed in a way that aims to make implementation of new metrics easy.
Currently statistical data is retrieved from Mattermost and GitLab, the tool itself aims to be service independent.

**Definitions**

Metric: A metric is defined by a specific question (e.g "How many $x from $service do $y") and the code necessary to answer it. This usually requires getting data from a service bf communicating with it's API and post-processing the data in order to answer the question at hand. Results are exported for further processing.

Please refer to the [extended documentation](docs/build/html/index.html) for further details about each metric.

## Examples
**How to add a new Metric**

Your team offers several internal courses, managed and hosted on a service named "scihub".
Scihub-courses implements various functions, one of them is giving users the ability to rate a course with stars (1-5)
Now lets assume your boss is interested in the courses which received 5 stars and which received less than 3 on average.
Here are the steps you need to do to add this requirement to statistics-collector.

Clone the repository and go to `statistics-collector/metrics` directory

Create a new file with a informative name, e.g `scihub-course-ratings.py`.
In this file, you will have to create a new class which inherits from the general 'Metric' class.
You can copy 'example_metric.py' provided in `/templates` to get started.
Keep in mind that `statistics-collector` does all its data processing with [pandas](https://pandas.pydata.org/).

```
from typing import Optional
import datetime
import pandas
from .metric import Metric

class ScihubCourseRatings(Metric):
    def __init__(self, host: str, token: str) -> None:
        super().__init__(host, token, accepts_date=False)
        self.host = host
        self.token = token

    # Implement further helper functions if needed

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        # Implement your custom data retrieval and processing here
        # No need to save your results, only return a pd.DataFrame, generate_statistics.py will handle the rest for you.

        #request the data from the API

        #do some cleansing (and calculations, if necessary) with pandas

        #return nice table as pandas.DataFrame()

```
**CI-Details**

Config files for each stage reside within `.gitlab/ci/`, which are included in the main `.gitlab-ci.yml`.
The `Makefile` contains project variables and commands triggered by specific stages.

Currently the output of stage:deploy is directly committed to the repository whereas the automatic docs created by stage:docs are stored as public artifacts and can be downloaded from the GUI.

**Manual documentation generation**

If, for any reason, you need to do this by hand:
```
poetry run sphinx-apidoc -o ./docs/source/ ./statistics_collector
cd docs && poetry run make html
```

Have a look at the [sphinx docs](https://www.sphinx-doc.org/en/master/) for further details.

**Getting historical data**

Per default data from yesterday is fetched by all metrics, on a daily basis. Sometimes one might need to get data from an arbitrary date further in the past. The class structure has this possible use-case built in and you can either execute it via Ipython or implement a new metric which supports these kinds of fetches (if the API does too).
Have a look into [metrics.py](statistics_collector/metrics/metric.py) for a more detailed description.

If you try to fetch data from the past by calling a metric, which hasn't implemented this (usually because the API itself doesn't offer this functionality) you will run into following error:
```
"[ERROR] $that_specific_metric doesn't support accessing data at an arbitrary date."
```

**Caution!!!**

As data is appended daily, files will grow over time - especially `gitlab_c_i_jobs.csv`.
Keep this in mind and make sure to add this functionality as soon as it`s clarified, which time-intervals make sense.

## Debugging

**How to test classes in IPython**
Especially during development of the data-processing part it can be quite helpful to see how your data behaves at each step - and play around on the fly. `IPython` is a great way to do that, but feel free to use any workflow that works for you!
Here is an example on how to import a metric class for further usage:


cd into `statistics-collector/statistics_collector` and start `ipython3`:

```
In [1]: from metrics.gitlab_merge_requests import GitlabMergeRequests

In [2]: a = GitlabMergeRequests("gitlab.hzdr.de", auth-token-comes-here).

In [3]: a.get_data()
[WARNING] No date specified. Getting data for yesterday
Out[3]:
   count      identifier                 recordedAt
0  14008  MERGE_REQUESTS  2022-02-14T23:50:10+01:00
```

### To-Dos
Mandatory and possible suggestions for further development

- adding more tests
- adding data visualization capabilities
- considering further modularization of certain functionalities (e.g for GraphQL queries) if more metrics are added over time
- improving CLI interaction
    - add option for conversion of user input to datetime.datetime.date
- see remaining issues for further information
- automatic cleaning of large files

## Resources

Below are some handy resource links:

* [Project Documentation](TODO)
* [Click](https://click.palletsprojects.com/en/7.x) is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary.
* [Sphinx](http://www.sphinx-doc.org/en/master/) is a tool that makes it easy to create intelligent and beautiful documentation, written by Geog Brandl and licensed under the BSD license.
* [pytest](https://docs.pytest.org/en/latest/) helps you write better programs.
* [GNU Make](https://www.gnu.org/software/make/) is a tool which controls the generation of executables and other non-source files of a program from the program's source files.

## Author Information

_HIFIS-Surveyval_ was created by
[HIFIS Software Services](https://software.hifis.net/).
